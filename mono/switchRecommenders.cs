namespace Autobahn {

  struct SwitchRecommendation {
    public int laneIndex;
    public double confidence;
  }

  abstract class SwitchRecommender : Recommender {
    public abstract SwitchRecommendation GetSwitchRecommendation();

    public SwitchRecommender(Driver driver) : base (driver) {
    }
  }

  class LaneSwitchRecommender : SwitchRecommender {
    public LaneSwitchRecommender(Driver driver) : base (driver){
    }

    public override string Name() {
      return "Lane";
    }

    public override SwitchRecommendation GetSwitchRecommendation()
    {
      SwitchRecommendation recommendation;
      recommendation.laneIndex = driver.GetInnerTrack();
      recommendation.confidence = 0.8;
      if (driver.NextIsSwitch())
      {
        recommendation.confidence = 1;
      }
      return recommendation;
    }
  }

  class BlockSwitchRecommender : SwitchRecommender {
    private const double blockThreshold = 50;

    public BlockSwitchRecommender(Driver driver) : base (driver){
    }

    public override string Name() {
      return "Block";
    }

    public override SwitchRecommendation GetSwitchRecommendation()
    {
      //Should block if enemy car is less than threshold away but greater than our car length away
      SwitchRecommendation recommendation;
	    PiecePosition enemyPosition;
	    double nearestBehindCarDistance = driver.NearestBehindCarDistance(out enemyPosition);
      recommendation.laneIndex = driver.GetInnerTrack();
      recommendation.confidence = 0.6;
	    if (nearestBehindCarDistance < blockThreshold && nearestBehindCarDistance > driver.OurCarLength){
	      recommendation.laneIndex = enemyPosition.lane.endLaneIndex;
	      recommendation.confidence = 0.85;
	    }
      return recommendation;
    }
  }

  class PassRivalSwitchRecommender : SwitchRecommender {
    private const double distanceThreshold = 50;

    public PassRivalSwitchRecommender(Driver driver) : base (driver){
    }

    public override string Name() {
      return "Pass";
    }

    public override SwitchRecommendation GetSwitchRecommendation()
    {
      // If nearest car ahead is less than threshold away, recommend switching innermost lane other than the rival's lane
      SwitchRecommendation recommendation;
	    PiecePosition enemyPosition;
	    double nearestBehindCarDistance = driver.NearestAheadCarDistance(out enemyPosition);
	    int secondBestLane;
      recommendation.laneIndex = driver.GetInnerTrack(out secondBestLane);
      recommendation.confidence = 0.6;

	    if (nearestBehindCarDistance < distanceThreshold) {
	      // Enemy is on the innerMost track
	      if (enemyPosition.lane.endLaneIndex == recommendation.laneIndex) {
	        recommendation.laneIndex = secondBestLane;
	        recommendation.confidence = 0.85; // Try and overtake nearest car ahead
	      } else {
	        recommendation.confidence = 0.9; // Definitely take the innermost track
	      }
	    }
      return recommendation;
    }
  }

}
