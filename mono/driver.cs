using System;
using System.Text;
using System.Collections.Generic;
using Autobahn.Hwo;

namespace Autobahn {

  public class Driver {
    Race race;
    ThrottleRecommender[] throttleRecommenders;
    SwitchRecommender[] switchRecommenders;
    TurboRecommender[] turboRecommenders;
    Dictionary<string, PiecePosition> currentCarPositions;
    int lastSwitchIndex;
    int currentTick;
    bool turboAvailable;
    string ourColor;

    public Race Race {
      set { this.race = value; }
    }

    public double DistanceLeftInStraightaway {
      get {
        if (IsOnTurn()) return 0;
        var distance = race.track.pieces[OurPosition.pieceIndex].length - OurPosition.inPieceDistance;
        var pieceIndex = (OurPosition.pieceIndex + 1) % race.track.pieces.Length;
        var piece = race.track.pieces[pieceIndex];
        while (piece.angle == 0) {
          distance += piece.length;
          pieceIndex = (pieceIndex + 1) % race.track.pieces.Length;
          piece = race.track.pieces[pieceIndex];
        }
        return distance;
      }
    }

    public PiecePosition OurPosition {
      get { return currentCarPositions[ourColor]; }
    }

    public string OurColor
    {
      get { return ourColor; }
      set { this.ourColor = value; }
    }

    public bool TurboAvailable
    {
      get { return turboAvailable; }
      set { this.turboAvailable = value; }
    }

    public int Tick
    {
      set { this.currentTick = value; }
    }

    public double OurCarLength
    {
      get {
        foreach (Car car in race.cars) {
          if (car.id.color.Equals(OurColor)) return car.dimension.length;
        }
        return -1;
      }
    }

    public double DistanceToNextTurn
    {
      get {
        var currentPieceIndex = OurPosition.pieceIndex;
        var pieceIndex = currentPieceIndex;
        var distance = 0.0;
        while (true) {
          var piece = race.track.pieces[pieceIndex];
          if (piece.angle == 0) {
            if (pieceIndex == currentPieceIndex) {
              distance += piece.length - PieceDistanceleft(ourColor);
            } else {
              distance += piece.length;
            }
            if (pieceIndex != race.track.pieces.Length - 1) {
              pieceIndex++;
            } else {
              pieceIndex = 0;
            }
          } else {
            return distance;
          }
        }
      }
    }

    public Driver() {
      lastSwitchIndex = -1;
      currentTick = 0;
      currentCarPositions = new Dictionary<string, PiecePosition>();

      throttleRecommenders = new ThrottleRecommender[] {
        new TurnThrottleRecommender(this),
            new StraightThrottleRecommender(this),
            new FinaleThrottleRecommender(this)};

      switchRecommenders = new SwitchRecommender[] {
        new LaneSwitchRecommender(this)};
        //new BlockSwitchRecommender(this),
        //new PassRivalSwitchRecommender(this)};

      turboRecommenders = new TurboRecommender[] {
        new StraightTurboRecommender(this)};
    }

    public double GetNextTurn() {
      double turnAngle = 0;
      bool turnStarted = false;
      for (int position = OurPosition.pieceIndex + 1; position != OurPosition.pieceIndex; position = (position + 1) % race.track.pieces.Length) {
        Track.Piece piece = race.track.pieces[position];
        if (piece.angle != 0) {
          turnAngle += piece.angle;
          turnStarted = true;
        } else if (turnStarted) {
          return turnAngle;
        }
        if ((position == race.track.pieces.Length-1) && IsOnLastLap()) break; //dont wrap if we are on last lap
      }
      return turnAngle;
    }

    // Returns the biggest sharpness of the next <numTurns> inclusive of the current turn
    public double GetNextTurnsSharpness(int numTurns=1) {
      int turns = 0;
      double biggestSharpness = 0;

      var pieceIndex = OurPosition.pieceIndex;
      while (turns < numTurns) {
        var piece = race.track.pieces[pieceIndex];
        if (piece.angle != 0) {
          var sharpness = Math.Abs(piece.angle / piece.radius);
          biggestSharpness = Math.Max(biggestSharpness, sharpness);
        }

        pieceIndex = (pieceIndex + 1) % race.track.pieces.Length;
        turns++;
      }

      return biggestSharpness;
    }

    // True if the next turn is left, false otherwise
    // Next excludes the current piece
    public bool IsNextTurnLeft() {
      return GetNextTurn() < 0;
    }

    // True if the next turn is right, false otherwise
    // Next excludes the current piece
    public bool IsNextTurnRight() {
      return GetNextTurn() > 0;
    }

    // True if there are no turns left before the finish line
    public bool IsNoTurnsBeforeFinish() {
      for (int position = OurPosition.pieceIndex; position < race.track.pieces.Length; position++) {
        if (race.track.pieces[position].angle != 0) {
          return false;
        }
      }

      return true;
    }

    // True if we are on our final lap
    public bool IsOnLastLap() {
      return OurPosition.lap == race.raceSession.laps - 1;
    }

    public int GetInnerTrack()
    {
      int secondInnerMostLane;
      return GetInnerTrack(out secondInnerMostLane);
    }

    // returns the inner most Lane for the next turn and ref to second Best. Overloaded method
    public int GetInnerTrack(out int secondInnerMostLane)
    {
      if (IsNextTurnLeft())
      {
        secondInnerMostLane = 1; //Assuming the tracks have at least two lanes
        return 0;
      }
      else if (IsNextTurnRight())
      {
        secondInnerMostLane = race.track.lanes.Length - 2; //Assuming the tracks have at least two lanes
        return race.track.lanes.Length - 1;
      }
      else
      {
        secondInnerMostLane = OurPosition.lane.endLaneIndex;
        return OurPosition.lane.endLaneIndex; // Stay at our current lane, no turns ahead
      }
    }

    // returns the outermost Lane for the next turn. TODO do we need this?
    public int GetOuterTrack()
    {
      if (IsNextTurnLeft())
      {
        return race.track.lanes.Length - 1;
      }
      else if (IsNextTurnRight())
      {
        return 0;
      }
      else
      {
        return (OurPosition.lane.endLaneIndex + 1) % race.track.lanes.Length; //Return any other lane besides our current one
      }
    }

    public bool NextIsSwitch() {
      int nextPieceIndex = (OurPosition.pieceIndex + 1) % race.track.pieces.Length;
      return race.track.pieces[nextPieceIndex].@switch;
    }

    public bool IsOnTurn() {
      return race.track.pieces[OurPosition.pieceIndex].angle != 0;
    }

    // Returns the distance to the nearest enemy ahead of us, with reference of that car's PiecePosition
    public double NearestAheadCarDistance(out PiecePosition carPosition) {
      return NearestCarDistance(out carPosition, (x) => IsAheadOfUs(x));
    }

    // Returns the distance to the nearest enemy behind us, with reference of that car's PiecePosition
    public double NearestBehindCarDistance(out PiecePosition carPosition)
    {
      return NearestCarDistance(out carPosition, (x) => !IsAheadOfUs(x));
    }

    public double NearestCarDistance(out PiecePosition carPosition, Func<PiecePosition, bool> directionPredicate) {
      double distance;
      double nearestDistance = Double.PositiveInfinity;
      carPosition = OurPosition;
      foreach (KeyValuePair<string, PiecePosition> car in currentCarPositions)
      {
        if (!car.Key.Equals(OurColor) && directionPredicate(car.Value)) {
	        distance = GetDistanceToCar(car.Key);
	        if (distance < nearestDistance) {
	          nearestDistance = distance;
	          carPosition = car.Value;
	        }
        }
      }

      return nearestDistance;
    }

    // Checks if the give car's position is ahead of us
    public bool IsAheadOfUs(PiecePosition car)
    {
      return car.lap > OurPosition.lap || car.pieceIndex > OurPosition.pieceIndex || car.inPieceDistance > OurPosition.inPieceDistance;
    }

    public double GetDistanceToCar(string carColor)
    {
      double distance = 0;
      int seek;
      int totalPieces = race.track.pieces.Length;
      if (IsAheadOfUs(currentCarPositions[carColor])) {
        seek = 1;
        distance += PieceDistanceleft(OurColor) + currentCarPositions[carColor].inPieceDistance;
      } else {
        seek = -1;
        distance += PieceDistanceleft(carColor) + OurPosition.inPieceDistance;
      }

      for (int position = (OurPosition.pieceIndex + seek) % totalPieces; position != currentCarPositions[carColor].pieceIndex; position = (position + seek) & totalPieces) {
        distance += race.track.pieces[position].length;
      }

      return distance;
    }

    // Returns the piece length of a given car's color
    public double CurrentPieceDistance(string carColor) {
      return race.track.pieces[currentCarPositions[carColor].pieceIndex].length;
    }

    // Returns the distance left in the current piece of a given car's color
    public double PieceDistanceleft(string carColor) {
      return CurrentPieceDistance(carColor) - currentCarPositions[carColor].inPieceDistance;
    }

    public SendMsg GetRecommendation() {
      currentTick++;

      // If we're approaching a switch, and haven't set our turn yet do it
      if (NextIsSwitch() && lastSwitchIndex != OurPosition.pieceIndex) {
        lastSwitchIndex = OurPosition.pieceIndex;
        return GetSwitchRecommendation();
      }

      // Only return turbo if it is recommended
      var turboRecommendation = GetTurboRecommendation();
      if (turboRecommendation != null) {
        TurboAvailable = false;
        return turboRecommendation;
      }

      return GetThrottleRecommendation();
    }

    SendMsg GetSwitchRecommendation()
    {
      double confidence = -1;
      StringBuilder breakdown = new StringBuilder("Tick ").Append(currentTick).Append(": ");

      int bestLane = 0;
      foreach (var recommender in switchRecommenders)
      {
        SwitchRecommendation recommendation = recommender.GetSwitchRecommendation();
        breakdown.Append(recommender.Name()).Append("(").Append(recommendation.laneIndex).Append(",").Append(recommendation.confidence).Append(")|");
        if (recommendation.confidence > confidence)
        {
          bestLane = recommendation.laneIndex;
        }
      }

      string direction = "";

      if (OurPosition.lane.endLaneIndex > bestLane)
      {
        direction = "Left";
      }
      else if (OurPosition.lane.endLaneIndex < bestLane)
      {
        direction = "Right";
      }

      breakdown.Append("=>").Append(direction);
      Console.WriteLine(breakdown.ToString());

      if (direction.Equals("")) {
        return new Ping(); // maybe return GetThrottleRecommendation();
      } else {
        return new SwitchLane(direction);
      }
    }

    SendMsg GetThrottleRecommendation() {
      double totalConfidence = 0;
      double weightedThrottle = 0;
      StringBuilder breakdown = new StringBuilder("Tick ").Append(currentTick).Append(": ");

      foreach (var recommender in throttleRecommenders) {
        var recommendation = recommender.GetThrottleRecommendation();
        totalConfidence += recommendation.confidence;
        weightedThrottle += recommendation.throttle * recommendation.confidence;

        breakdown.Append(recommender.Name()).Append("(").Append(Math.Round(recommendation.throttle, 2)).Append(",").Append(Math.Round(recommendation.confidence, 2)).Append(")|");
      }

      double recommendedThrottle = weightedThrottle / totalConfidence;

      breakdown.Append("=>").Append(recommendedThrottle);
      Console.WriteLine(breakdown.ToString());

      return new Throttle(recommendedThrottle);
    }

    SendMsg GetTurboRecommendation() {
      foreach (var recommender in turboRecommenders) {
        if (recommender.GetTurboRecommendation().shouldTurbo == true) {
          return new Turbo();
        }
      }
      return null;
    }

    public void UpdatePositions (IList<CarPosition> carPositions)
    {
      currentCarPositions.Clear();
      foreach (CarPosition car in carPositions) {
        currentCarPositions.Add(car.id.color, car.piecePosition);
      }
    }

    public PiecePosition GetCarPiecePosition(Car car) {
      PiecePosition position = currentCarPositions[car.id.color];
      return position;
    }

  }
}
