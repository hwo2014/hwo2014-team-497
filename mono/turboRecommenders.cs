namespace Autobahn {

  struct TurboRecommendation {
    public bool shouldTurbo;
  }

  abstract class TurboRecommender : Recommender {
    public abstract TurboRecommendation GetTurboRecommendation();

    public TurboRecommender(Driver driver) : base (driver) {
    }
  }

  class StraightTurboRecommender : TurboRecommender {
    public StraightTurboRecommender(Driver driver) : base (driver) {
    }

    public override string Name() {
      return "Straight";
    }

    public override TurboRecommendation GetTurboRecommendation() {
      TurboRecommendation recommendation;
      recommendation.shouldTurbo =
        driver.TurboAvailable &&
        driver.DistanceToNextTurn > 8000 ||
        driver.TurboAvailable &&
        driver.IsOnLastLap() &&
        driver.IsNoTurnsBeforeFinish();
      return recommendation;
    }
  }

}
