using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Sockets;
using Autobahn.Hwo;

namespace Autobahn {

  public class Application {

    private string host;

    private int port;

    private string botName;

    private string botKey;

    private string trackName;

    private int carCount;

    public static void Main(string[] args) {
      new Application(args);
    }

    public Application(string[] args) {
      AssignArguments(args);
      DisplayConnectingMessage();
      ConnectAndRunBot();
    }

    private void AssignArguments(string[] args) {
      if (args.Length < 4) throw new ArgumentException(
          "Not enough arguments to start application.");
      host = args[0];
      port = int.Parse(args[1]);
      botName = args[2];
      botKey = args[3];
      Console.WriteLine(
          string.Format("Started with arguments: {0}",
          string.Join(" ", args)));
      if (args.Length >= 6) {
        Console.WriteLine(trackName);
        trackName = args[4];
        carCount = int.Parse(args[5]);
      } else {
        trackName = "keimola";
        carCount = 1;
      }
    }

    private void DisplayConnectingMessage() {
      var message = string.Format(
          "Connecting to {0}:{1} as {2}/{3}",
          host, port, botName, botKey);
      Console.WriteLine(message);
    }

    private void ConnectAndRunBot() {
      using(var client = new TcpClient(host, port)) {
        var stream = client.GetStream();
        var reader = new StreamReader(stream);
        var writer = new StreamWriter(stream) { AutoFlush = true };
        var network = new Network(reader, writer);
        var bot = new Bot(network, botName, botKey, trackName, carCount);
        new Logger(network, bot.Driver);
        network.ReadAndHandleMessages();
      }
    }

  }

  public class Bot {

    private Driver driver = new Driver();

    private Network network;

    private Race race;

    public Driver Driver { get { return driver; } }

    public Bot(Network network, string botName, string botKey, string trackName, int carCount) {
      this.network = network;
      network.YourCarReceived += SetCar;
      network.GameInitReceived += SetRace;
      network.GameStartReceived += SendPing;
      network.CarPositionsReceived += SendThrottleOrSwitchLane;
      network.TurboAvailableReceived += SetTurboAvailable;
      SendJoin(botName, botKey, trackName, carCount);
    }

    private void SendJoin(string botName, string botKey, string trackName, int carCount) {
      network.Send(new Hwo.Join(botName, botKey));
    }

    private void SendPing(Object sender, EventArgs e) {
      network.Send(new Ping());
    }

    private void SendThrottleOrSwitchLane(Object sender, MessageReceivedEventArgs<IList<CarPosition>> e) {
      var carPositions = e.DeserializedObject;
      driver.UpdatePositions(carPositions);
      network.Send(driver.GetRecommendation());
    }

    private void SetCar(Object sender, MessageReceivedEventArgs<YourCar> e) {
      var car = e.DeserializedObject;
      driver.OurColor = car.color;
    }

    private void SetRace(Object sender, MessageReceivedEventArgs<GameInit> e) {
      driver.Race = e.DeserializedObject.race;
    }

    private void SetTurboAvailable(Object sender, MessageReceivedEventArgs<TurboAvailable> e) {
      driver.TurboAvailable = true;
    }

  }


  public class Logger {

    public Logger(Network network, Driver driver) {
      network.JoinReceived += (sender, e) => {
        Info(() => {
          Console.WriteLine("[1] Joined game");
        });
      };
      network.YourCarReceived += (sender, e) => {
        Info(() => {
          Console.WriteLine("[2] Car received");
        });
      };
      network.GameInitReceived += (sender, e) => {
        Info(() => {
          Console.WriteLine("[3] Game initialized");
        });
      };
      network.GameStartReceived += (sender, e) => {
        Info(() => {
          Console.WriteLine("[4] Game started");
        });
      };
#if LOG_CAR_POSITIONS_RECEIVED
      network.CarPositionsReceived += (sender, e) => {
        Info(() => {
          Console.WriteLine("[5] Car positions received");
        });
      };
#endif
      network.GameEndReceived += (sender, e) => {
        Info(() => {
          Console.WriteLine("[8] Game ended");
        });
      };
      network.TournamentEndReceived += (sender, e) => {
        Info(() => {
          Console.WriteLine("[9] Tournament ended");
        });
      };
      network.CrashReceived += (sender, e) => {
        if (e.DeserializedObject.color != driver.OurColor) return;
        Warn(() => {
          Console.WriteLine("[WARNING] Our car crashed!");
        });
      };
      network.SpawnReceived += (sender, e) => {
        if (e.DeserializedObject.color != driver.OurColor) return;
        Info(() => {
          Console.WriteLine("[INFO] We are back on the track");
        });
      };
      network.LapFinishedReceived += (sender, e) => {
        if (e.DeserializedObject.car.color != driver.OurColor) return;
        Info(() => {
          Console.WriteLine(string.Format(
              "[INFO] We finished lap {0} in {1} ms",
              e.DeserializedObject.lapTime.lap,
              e.DeserializedObject.lapTime.millis));
        });
      };
      network.DnfReceived += (sender, e) => {
        if (e.DeserializedObject.car.color != driver.OurColor) return;
        var reason = e.DeserializedObject.reason;
        Error(() => {
          Console.WriteLine("[ERROR] We have been disqualified!");
          Console.WriteLine(string.Format(
              "Reason: \"{0}\"",
              reason));
        });
      };
    }

    private static void Info(Action action) {
        var color = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor.Blue;
        action();
        Console.ForegroundColor = color;
    }

    private static void Warn(Action action) {
        var color = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor.Yellow;
        action();
        Console.ForegroundColor = color;
    }

    private static void Error(Action action) {
        var color = Console.ForegroundColor;
        Console.ForegroundColor = ConsoleColor.Red;
        action();
        Console.ForegroundColor = color;
    }

  }
}
