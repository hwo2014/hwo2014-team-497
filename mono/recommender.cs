namespace Autobahn {

  abstract class Recommender {
    protected Driver driver;
    public abstract string Name();

    public Recommender(Driver driver) {
      this.driver = driver;
    }
  }

}
