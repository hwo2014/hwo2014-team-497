using System;

namespace Autobahn {

  struct ThrottleRecommendation {
    public double throttle;
    public double confidence;
  }

  // Idea: perhaps we can do full throttle when we are sliding. I believe in the real world when drifting,
  // we should be giving gas when sliding to maintain center of gravity.
  abstract class ThrottleRecommender : Recommender {
    public abstract ThrottleRecommendation GetThrottleRecommendation();

    public ThrottleRecommender(Driver driver) : base (driver) {
    }
  }

  // Recommends the fastest possible speed for the car based on the current slip angle, angle of curve, and velocity.
  // Confidence is high at the start and middle of a curve, lowers towards the end so straightaway recommender takes over.
  class TurnThrottleRecommender : ThrottleRecommender {
    static int PIECE_LOOKAHEAD = 3;
    static double SHARPNESS_DAMPING = 1.85;
    static double BASE_THROTTLE = 0.65;

    public TurnThrottleRecommender(Driver driver) : base (driver) {
    }

    public override string Name() {
      return "Turn";
    }

    public override ThrottleRecommendation GetThrottleRecommendation() {

      ThrottleRecommendation recommendation;

      var sharpness = driver.GetNextTurnsSharpness(PIECE_LOOKAHEAD);
      recommendation.throttle = BASE_THROTTLE * (1 - sharpness / SHARPNESS_DAMPING);

      if (driver.IsOnTurn()) {
        recommendation.confidence = 1;
      } else {
        recommendation.confidence = Math.Max(0, (100 - driver.DistanceLeftInStraightaway) / 100);
      }

      return recommendation;
    }
  }

  // Handles throttle on straightaways
  // Goes at <startSpeed> Until there is only <decelerationPoint> distance left on the straightaway
  // Then we linearly interpolate the throttle down to <endSpeed> until we reach the <steadyPoint>
  // We travel at <endSpeed> from the <steadyPoint> to the end
  // Confidence is high at the start of a straightaway, lowers as we near the end so the turn recommender takes over.
  class StraightThrottleRecommender : ThrottleRecommender {
    static double startSpeed = 0.71;
    static double endSpeed = 0.64;
    static double decelerationPoint = 1000;
    static double steadyPoint = 400;
    static double decelerationInterval = decelerationPoint - steadyPoint;

    public override string Name() {
      return "Straight";
    }

    public StraightThrottleRecommender(Driver driver) : base (driver) {
    }

    public override ThrottleRecommendation GetThrottleRecommendation() {
      ThrottleRecommendation recommendation;
      var distanceLeft = driver.DistanceLeftInStraightaway;

      if (distanceLeft > decelerationPoint) {
        recommendation.throttle = startSpeed;
        recommendation.confidence = 1;
      } else if (distanceLeft < steadyPoint) {
        recommendation.throttle = endSpeed;
        recommendation.confidence = distanceLeft / decelerationPoint;
      } else {
        var distanceToSteady = distanceLeft - steadyPoint;
        var interpolationFactor = 1 - (distanceToSteady / decelerationInterval);
        recommendation.throttle = startSpeed + (endSpeed - startSpeed) * interpolationFactor;
        recommendation.confidence = distanceLeft / decelerationPoint;
      }

      //TODO handle negative recommendation confidence
      return recommendation;
    }
  }

  // If we are on the final lap and there are no more turns
  class FinaleThrottleRecommender : ThrottleRecommender {
    public override string Name() {
       return "Finale";
    }

    public FinaleThrottleRecommender(Driver driver) : base (driver) {
    }

    public override ThrottleRecommendation GetThrottleRecommendation() {
      ThrottleRecommendation recommendation;
      recommendation.throttle = 1.00;
      recommendation.confidence = 0;
      if (driver.IsOnLastLap() && driver.IsNoTurnsBeforeFinish()) {
        recommendation.confidence = 9000;
      }

      return recommendation;
    }
  }
}
