namespace Autobahn {

  public struct Race {
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;
  }

  // Race.Track
  public struct Track {
    public string id;
    public string name;
    public Piece[] pieces;
    public Lane[] lanes;

    public struct Lane {
      public int distanceFromCenter;
      public int index;
    }

    public struct Piece {
      public double length;
      public bool @switch;
      public double angle;
      public double radius;
    }
  }

  // Race.Car
  public class Car {
    public Id id;
    public Dimension dimension;

    public struct Id {
      public string name;
      public string color;
    }

    public struct Dimension {
      public double length;
      public double width;
      public double guideFlagPosition;
    }
  }

  // Race.RaceSession
  public struct RaceSession {
    public int laps;
    public int maxLapTimeMs;
    public bool quickRace;
  }

  public struct CarPosition {
    public Id id;
    public double angle;
    public PiecePosition piecePosition;

    public struct Id {
      public string name;
      public string color;
    }
  }

  public struct PiecePosition {
    public int pieceIndex;
    public double inPieceDistance;
    public Lane lane;
    public int lap;

    public struct Lane {
      public int startLaneIndex;
      public int endLaneIndex;
    }
  }

  public struct YourCar {
    public string name;
    public string color;
  }

  public struct Join {
    public string name;
    public string key;
  }

  public struct GameInit {
    public Race race;
  }

  public struct GameStart {
  }

  public struct GameEnd {
    public Result[] results;
    public BestLap[] bestLaps;

    public struct Result {
      public Car car;
      public CarResult result;

      public struct Car {
        public string name;
        public string color;
      }

      public struct CarResult {
        public int laps;
        public int ticks;
        public int millis;
      }
    }

    public struct BestLap {
      Car car;
      Result result;

      public struct Car {
        public string name;
        public string color;
      }

      public struct Result {
        public int lap;
        public int ticks;
        public int millis;
      }
    }
  }

  public struct TournamentEnd {
  }

  public struct TurboAvailable {
    public double turboDurationMilliseconds;
    public double turboDurationTicks;
    public double turboFactor;
  }

  public struct TurboStart {
    public string name;
    public string color;
  }

  public struct TurboEnd {
    public string name;
    public string color;
  }

  public struct Crash {
    public string name;
    public string color;
  }

  public struct Spawn {
    public string name;
    public string color;
  }

  public struct LapFinished {
    public Car car;
    public LapTime lapTime;
    public RaceTime raceTime;
    public Ranking ranking;

    public struct Car {
      public string name;
      public string color;
    }

    public struct LapTime {
      public int lap;
      public int ticks;
      public int millis;
    }

    public struct RaceTime {
      public int laps;
      public int ticks;
      public int millis;
    }

    public struct Ranking {
      public int overall;
      public int fastestLap;
    }
  }

  public struct Dnf {
    public Car car;
    public string reason;

    public struct Car {
      public string name;
      public string color;
    }
  }

  public struct Finish {
    public string name;
    public string color;
  }

}
