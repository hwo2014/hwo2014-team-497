using System;
using Newtonsoft.Json;

namespace Autobahn {
  namespace Hwo {

    public class MsgWrapper {
      public string msgType;
      public Object data;
      public int? gameTick;

      public MsgWrapper(string msgType, Object data) {
        this.msgType = msgType;
        this.data = data;
      }
    }

    public abstract class SendMsg {
      public string ToJson() {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
      }
      protected virtual Object MsgData() {
        return this;
      }

      protected abstract string MsgType();
    }

    public class Join: SendMsg {
      public string name;
      public string key;
      public string color;

      public Join(string name, string key) {
        this.name = name;
        this.key = key;
      }

      protected override string MsgType() {
        return "join";
      }
    }

    public class JoinRace: SendMsg {
      public Join botId;
      public string trackName;
      public int carCount;

     public JoinRace(string name, string key, string trackName, int carCount) {
       botId = new Join(name, key);
       this.botId.name = name;
       this.botId.key = key;
       this.trackName = trackName;
       this.carCount = carCount;
     }

      protected override string MsgType() {
        return "joinRace";
      }
    }

    public class Ping: SendMsg {
      protected override string MsgType() {
        return "ping";
      }
    }

    public class Throttle: SendMsg {
      public double value;

      public Throttle(double value) {
        this.value = value;
      }

      protected override Object MsgData() {
        return this.value;
      }

      protected override string MsgType() {
        return "throttle";
      }
    }

    public class SwitchLane: SendMsg {
      public string value;

      public SwitchLane(string value) {
        this.value = value;
      }

      protected override Object MsgData() {
        return this.value;
      }

      protected override string MsgType() {
        return "switchLane";
      }
    }

    public class Turbo : SendMsg {

      protected override Object MsgData() {
        return @"
         ___------__
  |\__-- /\       _ -
  |/_   __       _
  // \ /  \     /__
  |  o|  0|__      --_
  \\____-/ __ \   ___ -
  (@@   ___/  / /_
     -_____---   --_
     //  \ \\       -
   //|\__/  \\   ___ -
   \_\\_____/  \-|
       // \\--\|
  ____//  ||_
 /___//_\ /__|
";
      }

      protected override string MsgType() {
        return "turbo";
      }
    }

  }
}
