using System;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using Autobahn.Hwo;

namespace Autobahn {

  public class MessageReceivedEventArgs<T> : EventArgs {

    private T deserializedObject;

    private string json;

    private int? gameTick;

    public MessageReceivedEventArgs(T deserializedObject, string json, int? gameTick = null) {
      this.deserializedObject = deserializedObject;
      this.json = json;
      this.gameTick = gameTick;
    }

    public T DeserializedObject { get { return deserializedObject; } }

    public string Json { get { return json; } }

    public int? GameTick { get { return gameTick; } }

  }

  public class Network {

    private StreamReader reader;

    private StreamWriter writer;

    private IDictionary<string, Action<MsgWrapper>> dispatch;

    public Network(StreamReader reader, StreamWriter writer) {
      this.reader = reader;
      this.writer = writer;
      this.dispatch = new Dictionary<string, Action<MsgWrapper>>() {
        { "join", (msg) => { Dispatch<Join>(JoinReceived, msg); } },
        { "joinRace", (msg) => { Dispatch<Join>(JoinReceived, msg); } },
        { "yourCar", (msg) => { Dispatch<YourCar>(YourCarReceived, msg); } },
        { "gameInit", (msg) => { Dispatch<GameInit>(GameInitReceived, msg); } },
        { "gameStart", (msg) => { Dispatch<GameStart>(GameStartReceived, msg); } },
        { "carPositions", (msg) => { Dispatch<IList<CarPosition>>(CarPositionsReceived, msg); } },
        { "turboAvailable", (msg) => { Dispatch<TurboAvailable>(TurboAvailableReceived, msg); } },
        { "turboStart", (msg) => { Dispatch<TurboStart>(TurboStartReceived, msg); } },
        { "turboEnd", (msg) => { Dispatch<TurboEnd>(TurboEndReceived, msg); } },
        { "crash", (msg) => { Dispatch<Crash>(CrashReceived, msg); } },
        { "spawn", (msg) => { Dispatch<Spawn>(SpawnReceived, msg); } },
        { "lapFinished", (msg) => { Dispatch<LapFinished>(LapFinishedReceived, msg); } },
        { "finish", (msg) => { Dispatch<Finish>(FinishReceived, msg); } },
        { "dnf", (msg) => { Dispatch<Dnf>(DnfReceived, msg); } },
        { "gameEnd", (msg) => { Dispatch<GameEnd>(GameEndReceived, msg); } },
        { "tournamentEnd", (msg) => { Dispatch<TournamentEnd>(TournamentEndReceived, msg); } }
      };
    }

    public event EventHandler<MessageReceivedEventArgs<Join>> JoinReceived;

    public event EventHandler<MessageReceivedEventArgs<YourCar>> YourCarReceived;

    public event EventHandler<MessageReceivedEventArgs<GameInit>> GameInitReceived;

    public event EventHandler<MessageReceivedEventArgs<GameStart>> GameStartReceived;

    public event EventHandler<MessageReceivedEventArgs<IList<CarPosition>>> CarPositionsReceived;

    public event EventHandler<MessageReceivedEventArgs<TurboAvailable>> TurboAvailableReceived;

    public event EventHandler<MessageReceivedEventArgs<TurboStart>> TurboStartReceived;

    public event EventHandler<MessageReceivedEventArgs<TurboEnd>> TurboEndReceived;

    public event EventHandler<MessageReceivedEventArgs<Crash>> CrashReceived;

    public event EventHandler<MessageReceivedEventArgs<Spawn>> SpawnReceived;

    public event EventHandler<MessageReceivedEventArgs<LapFinished>> LapFinishedReceived;

    public event EventHandler<MessageReceivedEventArgs<Finish>> FinishReceived;

    public event EventHandler<MessageReceivedEventArgs<Dnf>> DnfReceived;

    public event EventHandler<MessageReceivedEventArgs<GameEnd>> GameEndReceived;

    public event EventHandler<MessageReceivedEventArgs<TournamentEnd>> TournamentEndReceived;

    public void ReadAndHandleMessages() {
      string line;
      while((line = reader.ReadLine()) != null) HandleMessage(line);
    }

    public void Send(SendMsg msg) {
      writer.WriteLine(msg.ToJson());
    }

    private void HandleMessage(string json) {
      var message = JsonConvert.DeserializeObject<MsgWrapper>(json);
      var type = message.msgType;
      try {
        dispatch[type](message);
      } catch (KeyNotFoundException e) {
        Console.WriteLine(string.Format("[WARNING] Message '{0}' has no handlers!", type));
      }
    }

    private void Dispatch<T>(EventHandler<MessageReceivedEventArgs<T>> handler, MsgWrapper message) {
      if (handler != null) {
        var json = message.data != null ? message.data.ToString() : "{}";
        var deserializedObject = JsonConvert.DeserializeObject<T>(json);
        var eventArgs = new MessageReceivedEventArgs<T>(deserializedObject, json, message.gameTick);
        handler(this, eventArgs);
      }
    }
  }
}
